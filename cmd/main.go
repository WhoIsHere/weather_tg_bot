package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"codeberg.org/WhoIsHere/weather_tg_bot/pkg"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"github.com/redis/go-redis/v9"
	"github.com/robfig/cron/v3"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	ctx := context.Background()
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: os.Getenv("REDIS_PWD"), // no password set
		DB:       0,                      // use default DB
	})

	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_BOT_SECRETE_KEY"))
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = false

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	c := cron.New()
	c = pkg.SendWeatherAtSevenFifteen(c, client, ctx, bot)
	c.Start()

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message updates
			continue
		}

		if !update.Message.IsCommand() { // ignore any non-command Messages
			continue
		}

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

		switch update.Message.Command() {
		case "start":
			msg.Text = "Hello. I am weather bot. /help for info"

		case "subscribe":
			key, err := client.Exists(ctx, fmt.Sprint(update.Message.Chat.ID)).Result()
			if err != nil {
				fmt.Println("Error checking key existence:", err)
			} else if key > 0 {
				msg.Text = "Already subscribed"

			} else {
				err := client.Set(ctx, fmt.Sprint(update.Message.Chat.ID), "1", 0).Err()
				if err != nil {
					panic(err)
				}

				msg.Text = "Successfully subscribed to daily weather"

			}

		case "unsubscribe":
			_, err := client.Del(ctx, fmt.Sprint(update.Message.Chat.ID)).Result()
			if err != nil {
				log.Print(err)
				msg.Text = "Sorry, an error occurred. Try again"
			}

			if err == nil {
				msg.Text = "Successfully unsubscribed"
			}

		case "help":
			msg.Text = "I understand /now, /hourly, /subscribe and /unsubscribe."
		case "now":
			weather := pkg.FetchWeatherForDay()

			current_weather_msg := pkg.CurrentWeatherMessageGenerator(weather.Current)
			msg.Text = current_weather_msg

		case "hourly":
			weather := pkg.FetchWeatherForDay()

			hourly_weather_msg := pkg.HourlyWeatherMessageGenerator(weather.Hourly)
			msg.Text = "Hourly"

			for index, m := range hourly_weather_msg {
				if index%3 == 0 {
					msg.Text += m + "\n"
				}
			}

		default:
			msg.Text = "I don't know that command"
		}

		if _, err := bot.Send(msg); err != nil {
			log.Panic(err)
		}

	}

	select {}
}
