package pkg

import (
	"context"
	"log"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/redis/go-redis/v9"
	"github.com/robfig/cron/v3"
)

func SendWeatherAtSevenFifteen(c *cron.Cron, rd *redis.Client, ctx context.Context, bot *tgbotapi.BotAPI) *cron.Cron {
	// "15 7 * * *",
	// "@every 10s"

	_, err := c.AddFunc("15 7 * * *", func() {
		iter := rd.Scan(ctx, 0, "*", 0).Iterator()

		for iter.Next(ctx) {
			userIdInInt64, err := strconv.ParseInt(iter.Val(), 10, 64)
			if err != nil {
				log.Fatal(err)
			}

			msg := tgbotapi.NewMessage(userIdInInt64, "")

			weather := FetchWeatherForDay()
			current_weather_msg := CurrentWeatherMessageGenerator(weather.Current)

			msg.Text = current_weather_msg

			if _, err := bot.Send(msg); err != nil {
				log.Panic(err)
			}
		}
		if err := iter.Err(); err != nil {
			panic(err)
		}
	})
	if err != nil {
		log.Fatal(err)
	}

	return c
}
