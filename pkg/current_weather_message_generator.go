package pkg

import (
	"fmt"
	"log"
	"time"
)

func CurrentWeatherMessageGenerator(weather Current) string {
	weather_current_time_raw, err := time.Parse("2006-01-02T15:04", weather.Time)
	if err != nil {
		log.Fatal("Error parsing date")
	}

	weather.PressureMsl = weather.PressureMsl / 1.33322

	weather_msg := fmt.Sprintf(`Current
				Time: %s
				Temperature: %.2f
				Apparent temperature: %.2f
				Precipitation: %.2f
				Wind speed: %.2f
				Pressure Msl: %.2f`, weather_current_time_raw.Format("02-01-2006 15:04"), weather.Temperature2M, weather.ApparentTemperature, weather.Precipitation, weather.WindSpeed10M, weather.PressureMsl)

	return weather_msg
}
