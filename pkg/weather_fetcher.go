package pkg

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

type Weather struct {
	Current Current `json:"current"`
	Hourly  Hourly  `json:"hourly"`
}

type Current struct {
	Time                string  `json:"time"`
	Temperature2M       float64 `json:"temperature_2m"`
	ApparentTemperature float64 `json:"apparent_temperature"`
	Precipitation       float64 `json:"precipitation"`
	PressureMsl         float64 `json:"pressure_msl"`
	SurfacePressure     float64 `json:"surface_pressure"`
	WindSpeed10M        float64 `json:"wind_speed_10m"`
}

type Hourly struct {
	Time                     []string  `json:"time"`
	WindSpeed10M             []float64 `json:"wind_speed_10m"`
	Temperature2M            []float64 `json:"temperature_2m"`
	ApparentTemperature      []float64 `json:"apparent_temperature"`
	PressureMsl              []float64 `json:"pressure_msl"`
	Precipitation            []float64 `json:"precipitation"`
	PrecipitationProbability []float64 `json:"precipitation_probability"`
	RelativeHumidity2M       []int     `json:"relative_humidity_2m"`
}

func FetchWeatherForDay() Weather {
	// requestURL := fmt.Sprintf("http://localhost:%s", city)

	requestURL := "https://api.open-meteo.com/v1/forecast?latitude=55.7522&longitude=37.6156&timezone=Europe%2FMoscow&wind_speed_unit=ms&current=apparent_temperature,precipitation,pressure_msl,surface_pressure,temperature_2m,wind_speed_10m&hourly=temperature_2m,apparent_temperature,precipitation_probability,precipitation,pressure_msl,wind_speed_10m"

	res, err := http.Get(requestURL)
	if err != nil {
		fmt.Printf("error making http request: %s\n", err)
		log.Fatalln(err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalln(err)
	}

	var result Weather
	if err := json.Unmarshal(body, &result); err != nil { // Parse []byte to go struct pointer
		fmt.Println("Can not unmarshal JSON")
	}

	return result
}
