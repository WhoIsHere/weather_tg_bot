package pkg

import (
	"fmt"
	"log"
	"time"
)

func HourlyWeatherMessageGenerator(weather Hourly) []string {
	var message []string

	for i := 0; i < len(weather.Time); i++ {
		weather_current_time_raw, err := time.Parse("2006-01-02T15:04", weather.Time[i])
		if err != nil {
			log.Fatal("Error parsing date")
		}

		location, err := time.LoadLocation("UTC")
		if err != nil {
			log.Fatal(err)
		}

		if weather_current_time_raw.After(time.Now().In(location)) && weather_current_time_raw.Before(time.Now().In(location).AddDate(0, 0, 1)) {

			weather.PressureMsl[i] = weather.PressureMsl[i] / 1.33322

			weather_msg := fmt.Sprintf(`
				Time: %s
				Temperature: %.2f
				Apparent temperature: %.2f
				Precipitation: %.2f
				Wind speed: %.2f
				Pressure Msl: %.2f`, weather_current_time_raw.Format("02-01-2006 15:04"), weather.Temperature2M[i], weather.ApparentTemperature[i], weather.Precipitation[i], weather.WindSpeed10M[i], weather.PressureMsl[i])

			message = append(message, weather_msg)

		}
	}
	return message
}
