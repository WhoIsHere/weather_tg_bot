module codeberg.org/WhoIsHere/weather_tg_bot

go 1.21.4

require github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/redis/go-redis/v9 v9.3.1 // indirect
	github.com/robfig/cron/v3 v3.0.0 // indirect
)
